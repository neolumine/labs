One downside of Kubuntu, KDE, and KIO is that SMB shares mounted in the file manager aren't accessible from the terminal (or any other software that doesn't understand KIO). GNOME's GVFS does not have this limitation. Here is how to install and use it on Kubuntu.

Install the following:

```
sudo apt install thunar gvfs-backends gvfs-fuse
```

Then open Thunar and navigate to network shares as usual. It should work just fine to open files on those shares in any application. From the terminal, you should see mounted shares present under under `/run/user/<uid>/gvfs`.